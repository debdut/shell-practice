# Shell Log

In **practice/**

Created Directory **shell-playground/**

`$ mkdir shell-playground`

- mkdir => Make Directory

Entering Directory **shell-playground/**

`$ cd shell-playground`

- cd => Change Directory

Get Present Working Directory

`$ pwd`

- pwd => Present Working Directory

Tag the result as *%1*

This gives us exact location of the directory we are in

Lets go to root, **/** => root directory

`$ cd /`

This directory contains everything in the OS

Get Present Working Directory

`$ pwd`

Lets see whats in this directory

`$ ls`

- ls => list

Lets move around, entering **bin**

`$ cd bin`

List again

`$ ls`

Lots of Files, lets go back

`$ cd ..`

- .. => Parent Directory
- . => Current Directory
- ~ => Home Directory

Lets go home

`$ cd ~`

Get Location

`$ pwd`

Tag this as *%2*

**/home/debdut** => **root** then **home** directory then **debdut** directory

Lets go back to playground, use *%1*

`$ cd %1`

List contents

`$ ls`

It should be empty

Lets create 3 directories **games**, **movies**, **songs**

`$ mkdir games movies songs`

List contents

`$ ls`

Now you see 3 directories

Now lets create another directory in **games**, **strategy**

`$ mkdir games/strategy`

Lets list contents of **games**

`$ ls games`

Now lets create **fiction** within **books**

```
$ mdkir books
$ mkdir books/fiction
```

Possible Wrong Answer

`$ mkdir books/fiction`

This gives an error

`mkdir: cannot create directory ‘books/fiction’: No such file or directory`

Shortcut

`$ mkdir -p books/fiction`

Lets Remove **movies**

`$ rmdir movies`

- rmdir => Remove Directory

Check by `ls` to see its removed

Lets try to remove **books**

```
$ rmdir books/fiction
$ rmdir books
```

Possible Wrong Solution

`$ rmdir books`

This gives an error

`rmdir: failed to remove 'books': Directory not empty`

Shortcut

`$rmdir -p books/fiction`

Lets create some files **temp**, **hello**, **script**, **log**

`$ touch temp hello script log`

**Directory is a special type of File. It contains more files.**

Lets see which are directories and which are normal files

`$ ls -l`

- Minus ( - ) => Options or Flags
- ls -l => Long List

Output

```
total 8
drwxr-xr-x 3 debdut debdut 4096 Jul 20 13:21 games
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 hello
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 log
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 script
drwxr-xr-x 2 debdut debdut 4096 Jul 20 13:19 songs
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 temp
```

See the first column, one starting with **d** is a directory, rest are files

You can also see the time and date when the files are created

More Options

`$ ls -lt`

- ls -lt => Long list sorted by time

Output

```
total 8
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 hello
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 log
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 script
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 temp
drwxr-xr-x 3 debdut debdut 4096 Jul 20 13:21 games
drwxr-xr-x 2 debdut debdut 4096 Jul 20 13:19 songs
```

`$ ls -lrt`

- ls -lrt => Long list sorted by time reversed

Output

```
total 8
drwxr-xr-x 2 debdut debdut 4096 Jul 20 13:19 songs
drwxr-xr-x 3 debdut debdut 4096 Jul 20 13:21 games
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 temp
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 script
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 log
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 hello
```

Lets create some hidden files

**Files starting with . are hidden**

`$ touch .mongo`

List to see that its not visible

`$ ls -a`

- ls -a => List all files (hidden too)

Output

```
.  ..  games  hello  log  .mongo  script  songs  temp
```

We can create hidden directories too

`$ mkdir .ucantseeme`

Lets list them all!

`$ ls -al`

Output

```
total 16
drwxr-xr-x 4 debdut debdut 4096 Jul 20 14:38 .
drwxr-xr-x 3 debdut debdut 4096 Jul 20 12:48 ..
drwxr-xr-x 3 debdut debdut 4096 Jul 20 13:21 games
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 hello
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 log
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:38 .mongo
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 script
drwxr-xr-x 2 debdut debdut 4096 Jul 20 13:19 songs
-rw-r--r-- 1 debdut debdut    0 Jul 20 14:13 temp
```

Lets take a break!!!